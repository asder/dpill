```sh
$ npm install
```
Build app: 
```sh
$ npm run build
```
Start node after build
```sh
$ npm run start
```
Build and start node in inspect mode
```sh
$ npm run dev
```
Start 4 nodes (Windows only)
```sh
$ startNodes.bat
```
Test matrix
```sh
$ npm run matrix
```