//const size = 2 * Math.round(Math.random() * 10000) - 1;
const size = 5;
const matrix = Array.from(Array(size)).map(() => Array.from(Array(size)).map(() => Math.round(Math.random() * 90 + 10)));

const rays = [];
const center = (size - 1)>>1;
const initialVec = {x: 0 - center, y: center - center};
initialVec.length = Math.sqrt(initialVec.x**2 + initialVec.y**2);
const minAngleVec = {x: initialVec.x, y: initialVec.y + 1};
minAngleVec.length = Math.sqrt(minAngleVec.x**2 + minAngleVec.y**2);
const minAngle = Math.acos((initialVec.x * minAngleVec.x + initialVec.y * minAngleVec.y) / initialVec.length / minAngleVec.length);
const maxLengthVec = {x: 0 - center, y: 0 - center};
maxLengthVec.length = Math.sqrt(maxLengthVec.x**2 + maxLengthVec.y**2);
const PI2 = Math.PI * 2;
matrix.forEach((row, y) => {
    console.log(row.join(' '));
    row.forEach((value, x) => {
        const curVec = {x: x - center, y: y - center};
        curVec.length = Math.sqrt(curVec.x**2 + curVec.y**2);
        let sortMetric;
        if(!curVec.length) {
            sortMetric = 0;
        } else {
            const angle = Math.acos((initialVec.x * curVec.x + initialVec.y * curVec.y) / initialVec.length / curVec.length);
            sortMetric = (y > center ? PI2 - angle : angle) + (curVec.length / maxLengthVec.length) * minAngle;
        }
        rays.push({value, sortMetric});
    });
});
rays.sort((a, b) => a.sortMetric - b.sortMetric);
console.log('\n', rays.map(({value}) => value).join(' '))