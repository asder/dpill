import {
    messagesStreamName,
    groupName,
    messageInterval,
    publisherStreamName,
    publisherTimeout,
    nodeName,
    publisherMessageId, numMessagesPerRequest
} from './config';

export class Publisher {
    private pendingTimeout: any = 0;
    private keepAliveTimeout: any = 0;
    private workingPromise: Promise<any> = Promise.resolve();
    private workingPromiseResolve = () => {};

    constructor(private redis: any) {
    }

    start(): Promise<any> {
        return this.workingPromise = new Promise(async(resolve) => {
            this.keepAlive();
            this.workingPromiseResolve = resolve;
            // send first data to be sure the stream is created
            await this.sendData();
            try {
                // be sure working group is created
                await this.redis.xgroup('CREATE', messagesStreamName, groupName, 0);
            } catch (e) {
                console.log(`Working group is created`, e);
            }
            this.repeatData();
        });
    }

    stop() {
        clearTimeout(this.pendingTimeout);
        clearTimeout(this.keepAliveTimeout);
        this.workingPromiseResolve();
    }

    private repeatData() {
        this.pendingTimeout = setTimeout(async() => {
            await this.sendData();
            this.repeatData();
        }, messageInterval);
    }

    private async sendData() {
        const value = Array(80 + Date.now()%21).fill('a').join('');
        try {
            await this.redis.xadd(messagesStreamName, '*', 'fieldName', value);
            console.log('Published message');
        } catch (e) {
            console.log(e);
        }
    }

    private keepAlive() {
        this.keepAliveTimeout = setTimeout(async() => {
            try {
                //drop timeout
                const message = await this.redis.xclaim(publisherStreamName, groupName, nodeName, 0, publisherMessageId);
                /*if(!message.length) {
                    return this.stop();
                }*/
            } catch (e) {
                console.log(e);
            }
            this.keepAlive();
        }, publisherTimeout/2);
    }
}