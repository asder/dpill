import {
    redisUrl,
    redisPassword,
    redisPort,
    publisherStreamName,
    groupName,
    numMessagesPerRequest,
    nodeName,
    publisherMessageId,
    errorListName
} from './config';
import IORedis from 'ioredis';

import { Publisher } from './publisher';
import { Worker } from './worker';

const redis = new IORedis({
    port: redisPort,
    host: redisUrl,
    password: redisPassword
});

if(process.argv[2] === 'getErrors') {
    getErrors();
    process.exit();
} else {
    main();
}

async function getErrors() {
    let message
    while(message = await redis.lpop(errorListName)) {
        console.log(message);
    }
}

async function main() {
    try {
        try {
            await redis.xadd(publisherStreamName, publisherMessageId, 'key', 'master');
        } catch (e) {
            console.log('Publisher message exists', e);
        }

        try {
            // be sure publisher group is created
            await redis.xgroup('CREATE', publisherStreamName, groupName, 0);
        } catch (e) {
            console.log(`Publisher group is created`, e);
        }

        try {
            //set pending status on publisher stream message if not
            const streams = await redis.xreadgroup(
                'GROUP', groupName,
                nodeName,
                'COUNT', numMessagesPerRequest,
                'STREAMS', publisherStreamName,
                '>'
            );
        } catch {}

        console.log(`Node started recieve messages`);
        const worker = new Worker(redis);
        await worker.start();

        console.log(`Node started publishing messages`);
        const publisher = new Publisher(redis);
        await publisher.start();

    } catch (e) {
        console.log(e);
    }
}


