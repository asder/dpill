import {
    messagesStreamName,
    groupName,
    numMessagesPerRequest,
    errorListName,
    messageInterval,
    pendingMessagesTimeout,
    publisherStreamName,
    publisherTimeout,
    nodeName,
    publisherMessageId
} from './config';

export class Worker {
    private pendingTimeout: any = 0;
    private workingPromise: Promise<any> = Promise.resolve();
    private workingPromiseResolve = () => {};

    constructor(private redis: any) {
    }

    start(): Promise<any> {
        return this.workingPromise = new Promise((resolve) => {
            this.workingPromiseResolve = resolve;
            this.retrieveMessages(0);
        });
    }

    stop() {
        clearTimeout(this.pendingTimeout);
        this.workingPromiseResolve();
    }

    private retrieveMessages(wait: number) {
        this.pendingTimeout = setTimeout(async() => {
            let redisHasMessages = false;
            try {
                const pending = await this.redis.xpending(messagesStreamName, groupName, '-', '+', numMessagesPerRequest);
                pending && pending.forEach(async([messageID, consumer, downtime]: [string, string, number]) => {
                    if(downtime > pendingMessagesTimeout) {
                        redisHasMessages = true;
                        try {
                            const messages = await this.redis.xclaim(messagesStreamName, groupName, nodeName, pendingMessagesTimeout, messageID);
                            if(messages.length) {
                                this.solveMessages(messages);
                            }
                        } catch (e) {
                            console.log(e);
                        }
                    }
                });

                const streams = await this.redis.xreadgroup(
                    'GROUP', groupName,
                    nodeName,
                    'COUNT', numMessagesPerRequest,
                    'STREAMS', messagesStreamName,
                    '>'
                );
                streams && streams.forEach((stream: [string, []]) => {
                    const [streamName, messages] = stream;
                    if(messages.length) {
                        redisHasMessages = true;
                        switch (streamName) {
                            case messagesStreamName:
                                this.solveMessages(messages);
                                break;
                            default:
                                console.log(`Unknown stream ${streamName}`);
                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
            //check for publisher
            if(!redisHasMessages) {
                try {
                    const [[messageId, ownerId, downtime]] = await this.redis.xpending(publisherStreamName, groupName, '-', '+', numMessagesPerRequest);
                    if(downtime > publisherTimeout) {
                        const message = await this.redis.xclaim(publisherStreamName, groupName, nodeName, publisherTimeout, publisherMessageId);
                        if(message.length) {
                            return this.stop();
                        }
                    }
                } catch {}
            }
            this.retrieveMessages(redisHasMessages ? 0 : messageInterval)
        }, wait);
    }

    private solveMessages(messages: any[]) {
        messages.forEach(async ([messageID, data]: [string, []]) => {
            if (Math.floor(Math.random() * Math.floor(100)) <= 5) {
                // was an error, save to the list
                await this.redis.rpush(errorListName, JSON.stringify([messageID, data]));
            }
            await this.redis.xack(messagesStreamName, groupName, messageID);
        });
    }
}